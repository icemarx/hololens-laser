using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;
using HoloToolkit.Unity.InputModule;

public class Shoot : MonoBehaviour {
    public GameObject bullet;
    public Transform gun;
    public float shootForce = 0f;
    public GestureRecognizer rcgn;
    /*
	void Update() {
        if(Input.GetMouseButtonDown(0)) {   // activates when mouse clicked, works in editor
            GameObject go = (GameObject)Instantiate(bullet, gun.position, gun.rotation);
            go.GetComponent<Rigidbody>().AddForce(gun.forward * shootForce);
        }
	}
    */
    public static Shoot Instance { get; private set; }

    void Awake() {
        Instance = this;
        rcgn = new GestureRecognizer();
        // Debug.Log(rcgn);
        rcgn.StartCapturingGestures();
        // rcgn.SetRecognizableGestures(GestureSettings.Tap);

        // fire bullets
         rcgn.Tapped += (args) => {     // this event doesn't work in Unity editor
            // Debug.Log("bang");
            GameObject go = Instantiate(bullet, gun.position, gun.rotation);
            go.GetComponent<Rigidbody>().AddForce(gun.forward * shootForce);
        };
        rcgn.StartCapturingGestures();
    }

}
