using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    // Use this for initialization
    public float expiryTime = 0f;
    private void Start() {
        Destroy(gameObject, expiryTime);
    }

    // Update is called once per frame
    void Update () {
        
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject != GameObject.Find("MixedRealityCamera")) {
            Debug.Log("bullet collided");
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}