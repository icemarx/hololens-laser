using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor_Spawning : MonoBehaviour {
    public GameObject[] meteor = new GameObject[2];
    public float distance;      // the radius of meteor spawn locations
    public float min_dis;
    public float wave_time;
    public int num_spawned;

    private void Start() {
        for(int i = 0; i < num_spawned; i++)
            // after 5s, spawn n meteors every t seconds
            InvokeRepeating("Spawn", 5.0f, wave_time);
    }
    
    void Spawn() {
        Vector3 rand = distance * Random.insideUnitCircle;
        while (rand.magnitude < min_dis)
            rand = distance * Random.insideUnitCircle;

        Vector3 offset = new Vector3(rand.x, 0, rand.y);
        GameObject go = (GameObject)Instantiate(meteor[Random.Range(0, 3)], transform.position + offset, transform.rotation);

        Debug.Log("Meteor spawned at " + go.transform.position);
    }

    /*
    void Update() {
        // used for testing purposes
        if (Input.GetKeyDown(KeyCode.Space)) {  // change this later to automatic spawning based on time
            
            Vector3 rand = distance*Random.insideUnitCircle;
            while(rand.magnitude < min_dis)
                rand = distance*Random.insideUnitCircle;

            Vector3 offset = new Vector3(rand.x, 0, rand.y);
            GameObject go = (GameObject)Instantiate(meteor[Random.Range(0, 3)], transform.position + offset, transform.rotation);
        }
    }
    */
}