using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor_Movement : MonoBehaviour {

    public float expiryTime = 25f;

    public float velocity = 0f;
    public float spin_max = 0f;
    public float spin_min = 0f;

    private float spin_up = 0f;
    private float spin_right = 0f;
    private float spin_forward = 0f;

    private Rigidbody rigb;
    private Vector3 dir;        // enotski vektor v smeri igralca
    
    void Start() {
        // makes sure the meteor is destroyed after some time
        Destroy(gameObject, expiryTime);

        rigb = GetComponent<Rigidbody>();
        Vector3 player_position = GameObject.Find("MixedRealityCamera").transform.position;
        Vector3 meteor_position = transform.position;
        dir = (player_position-meteor_position)/(player_position-meteor_position).magnitude;

        spin_up = Random.Range(spin_min, spin_max);
        spin_right = Random.Range(spin_min, spin_max);
        spin_forward = Random.Range(spin_min, spin_max);

        rigb.AddTorque(transform.up * spin_up, ForceMode.VelocityChange);
        rigb.AddTorque(transform.right * spin_right, ForceMode.VelocityChange);
        rigb.AddTorque(transform.forward * spin_forward, ForceMode.VelocityChange);
    }

    void FixedUpdate() {
        rigb.AddForce(velocity*dir);
    }
}